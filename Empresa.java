/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author miljeveco
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 * @author Colaborador: (Anthony Ardila 11519)
 */
public class Empresa {

    protected double aporteSalud;
    protected double aportePensiones;
    protected double totalConsignarSalud;
    protected double totalConsignarPensiones;
    protected double totalProvisionCesantias;
    protected double totalPrimas;
    protected double totalPagarEmpleados;    
    protected int totalEmpleadosLiquidables;
    protected Empleado[] empleados;
    private int totalEmpleados;

    public Empresa(){
        //COMPLETE
        this.empleados = new Empleado[100];
    }

    /**
     * @return the aporteSalud
     */
    public double getAporteSalud() {
        return aporteSalud;
    }

    /**
     * @param aporteSalud the aporteSalud to set
     */
    public void setAporteSalud(double aporteSalud) {
        this.aporteSalud = aporteSalud;
    }

    /**
     * @return the aportePensiones
     */
    public double getAportePensiones() {
        return aportePensiones;
    }

    /**
     * @param aportePensiones the aportePensiones to set
     */
    public void setAportePensiones(double aportePensiones) {
        this.aportePensiones = aportePensiones;
    }

    /**
     * @return the totalConsignarSalud
     */
    public double getTotalConsignarSalud() {
        return totalConsignarSalud;
    }

    /**
     * @param totalConsignarSalud the totalConsignarSalud to set
     */
    public void setTotalConsignarSalud(double totalConsignarSalud) {
        this.totalConsignarSalud = totalConsignarSalud;
    }

    /**
     * @return the totalConsignarPensiones
     */
    public double getTotalConsignarPensiones() {
        return totalConsignarPensiones;
    }

    /**
     * @param totalConsignarPensiones the totalConsignarPensiones to set
     */
    public void setTotalConsignarPensiones(double totalConsignarPensiones) {
        this.totalConsignarPensiones = totalConsignarPensiones;
    }

    /**
     * @return the totalProvisionCesantias
     */
    public double getTotalProvisionCesantias() {
        return totalProvisionCesantias;
    }

    /**
     * @param totalProvisionCesantias the totalProvisionCesantias to set
     */
    public void setTotalProvisionCesantias(double totalProvisionCesantias) {
        this.totalProvisionCesantias = totalProvisionCesantias;
    }

    /**
     * @return the totalPrimas
     */
    public double getTotalPrimas() {
        return totalPrimas;
    }

    /**
     * @param totalPrimas the totalPrimas to set
     */
    public void setTotalPrimas(double totalPrimas) {
        this.totalPrimas = totalPrimas;
    }

    /**
     * @return the totalPagarEmpleados
     */
    public double getTotalPagarEmpleados() {
        return totalPagarEmpleados;
    }

    /**
     * @param totalPagarEmpleados the totalPagarEmpleados to set
     */
    public void setTotalPagarEmpleados(double totalPagarEmpleados) {
        this.totalPagarEmpleados = totalPagarEmpleados;
    }

    /**
     * Este metodo llama el metodo del mismo nombre en la clase Empleado, dentro de un ciclo for por cada 
     * objeto empleado dentro del arreglo o ArrayList
     */    
    public void liquidarNomina(Fecha fechaLiquidacion) {
        //COMPLETE
        double salario = 0;
        double salud = 0;
        double pension = 0;
        double cesantia = 0;
        double prima = 0;
        double total = 0;
        int total_empleados = 0;
        for(Empleado e : this.empleados){
            if(e != null && e.esEmpleadoLiquidable(fechaLiquidacion)){
                e.liquidarEmpleado(fechaLiquidacion);
                
                salario += e.getSalarioBase();
                salud += e.getDescuentoSalud();
                pension += e.getDescuentoPension();
                cesantia += e.getProvisionCesantias();
                prima += e.getPrima();
                total += e.getTotalAPagar();
                
                total_empleados++;
            }
            
        }
        this.setAporteSalud(salario * 8.5 / 100);
        this.setAportePensiones(salario * 12 / 100);
        this.setTotalConsignarSalud(salud + this.getAporteSalud());
        this.setTotalConsignarPensiones(pension + this.getAportePensiones());
        this.setTotalProvisionCesantias(cesantia);
        this.setTotalPrimas(prima);
        this.setTotalPagarEmpleados(total);
        this.setTotalEmpleadosLiquidables(total_empleados);
    }

    public double getCostoNomina() {
        return this.getTotalConsignarSalud() + this.getTotalConsignarPensiones() + 
                this.getTotalProvisionCesantias() + this.getTotalPagarEmpleados();//COMPLETE
    }

    public int getTotalEmpleados() {
         int p = 0;
        while(this.empleados[p]!=null) p++;
        return p;
    }

    public int getTotalEmpleadosActivos() {
        int total = 0;
        for(Empleado e : this.empleados) {
            if(e!= null && e.esEmpleadoActivo()) total++;
        }
        return total;//COMPLETE
    }

    public int getTotalEmpleadosValidos() {
       int total = 0;
        for(Empleado e : this.empleados) {
            if(e!= null && e.esEmpleadoValido()) total++;
        }
        return total;//COMPLETE
    }    

    public int getTotalEmpleadosActivosYValidos() {
        int total = 0;
        for(Empleado e : this.empleados) {
            if(e!= null && e.esEmpleadoActivo() && e.esEmpleadoValido()) total++;
        }
        return total;//COMPLETE
    }

    /**Metodo de acceso a la propiedad empleados*/
    public Empleado[] getEmpleados(){
        return this.empleados;
    }//end method getEmpleados

    /**Metodo de modificación a la propiedad empleados*/
    public void setEmpleados(Empleado[] empleados){
        this.empleados = empleados;
    }//end method setEmpleados

    /**Metodo de acceso a la propiedad totalEmpleadosLiquidables*/
    public int getTotalEmpleadosLiquidables(){
        return this.totalEmpleadosLiquidables;
    }//end method getTotalEmpleadosLiquidables

    /**Metodo de modificación a la propiedad totalEmpleadosLiquidables*/
    public void setTotalEmpleadosLiquidables(int totalEmpleadosLiquidables){
        this.totalEmpleadosLiquidables = totalEmpleadosLiquidables;
    }//end method setTotalEmpleadosLiquidables

    /**
     * Agrega un Gerente a la lista de empleados de la empresa
     */
    public void agregarGerente(String cedula, String nombres, String apellidos, double salarioBase, int fechaIngreso, int fechaRetiro){
        //COMPLETE
         Gerente g = new Gerente(cedula, nombres, apellidos, salarioBase, fechaIngreso, fechaRetiro);
        this.agregarEmpleado(g);
    }

    /**
     * Agrega un Operario a la lista de empleados de la empresa
     */
    public void agregarOperario(String cedula, String nombres, String apellidos, double salarioBase, int fechaIngreso, int fechaRetiro){
        //COMPLETE
        Operario o = new Operario(cedula, nombres, apellidos, salarioBase, fechaIngreso, fechaRetiro);
        this.agregarEmpleado(o);
    }    

    /**
     * Agrega un Empleado a la lista de empleados de la empresa
     */
    public void agregarEmpleado(Empleado e){
        //COMPLETE
        int p = 0;
        while(this.empleados[p]!=null) p++;
        this.empleados[p] = e;
    }
}