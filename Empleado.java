/*
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 * @author Colaborador: (Anthony Ardila 11519)
 */

public abstract class Empleado {

    protected String cedula;
    protected String apellidos;
    protected String nombres;
    protected double salarioBase;
    protected double descuentoSalud;
    protected double descuentoPension;
    protected double provisionCesantias;
    protected double prima;
    protected double ingresos;
    protected double descuentos;
    protected double totalAPagar;
    protected Fecha fechaIngreso;
    protected Fecha fechaRetiro;

    /**
     * Constructor default
     */
    public Empleado() {
        //COMPLETE        
    }

    /**
     * Constructor con parámetros. Recibe todos los datos del empleado
     */
    public Empleado(String cedula, String nombres, String apellidos, double salarioBase, int fechaIngreso, int fechaRetiro){
        //COMPLETE
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.salarioBase = salarioBase;
        this.fechaIngreso = new Fecha(fechaIngreso);
        if(fechaRetiro > 0) this.fechaRetiro = new Fecha(fechaRetiro);
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the salarioBase
     */
    public double getSalarioBase() {
        return salarioBase;
    }

    /**
     * @param salarioBase the salarioBase to set
     */
    public void setSalarioBase(double salarioBase) {
        this.salarioBase = salarioBase;
    }

    /**
     * @return the descuentoSalud
     */
    public double getDescuentoSalud() {
        return this.salarioBase*4/100;
    }

    /**
     * @param descuentoSalud the descuentoSalud to set
     */
    public void setDescuentoSalud(double descuentoSalud) {
        this.descuentoSalud = descuentoSalud;
    }

    /**
     * @return the descuentoPension
     */
    public double getDescuentoPension() {
        return this.salarioBase*4/100;
    }

    /**
     * @param descuentoPension the descuentoPension to set
     */
    public void setDescuentoPension(double descuentoPension) {
        this.descuentoPension = descuentoPension;
    }

    /**
     * @return the provisionCesantias
     */
    public double getProvisionCesantias() {
        return this.salarioBase/12;
    }

    /**
     * @param provisionCesantias the provisionCesantias to set
     */
    public void setProvisionCesantias(double provisionCesantias) {
        this.provisionCesantias = provisionCesantias;
    }

    /**
     * @return the prima
     */
    public double getPrima() {
        return prima;
    }

    /**
     * @param prima the prima to set
     */
    public void setPrima(double prima) {
        this.prima = prima;
    }

    public void setFechaIngreso(int fecha) {
        this.fechaIngreso = new Fecha(fecha);
    }

    public void setFechaRetiro(int fecha) {
        this.fechaRetiro = new Fecha(fecha);
    }

    /**
     * En este metodo se liquida a un unico empleado, de manera que desde la clase empresa
     * se llame a este metodo por cada empleado dentro del array o ArrayList
     */
    public void liquidarEmpleado(Fecha fechaLiquidacion) {
        //COMPLETE
        if(this.esEmpleadoLiquidable(fechaLiquidacion)){
            int a = fechaLiquidacion.getAnio() - this.fechaIngreso.getAnio();
            double prima = 0;
            if(fechaLiquidacion.getMes() == 06 || fechaLiquidacion.getMes() == 12){
                prima += this.salarioBase * 50 / 100;
            } 
            if(a > 0 && fechaLiquidacion.getMes() == this.fechaIngreso.getMes()){
                if(a <= 5) {
                    prima += this.salarioBase * a * 5 / 100;
                } else {
                    prima += this.salarioBase * 30 / 100;
                }
            }
            this.setPrima(prima);
            this.setIngresos(this.getSalarioBase() + this.getPrima());
            this.setDescuentos(this.getDescuentoSalud() + this.getDescuentoPension());
            this.setTotalAPagar(this.getIngresos() - this.getDescuentos());
        }
    }

    public boolean esEmpleadoActivo() {
        boolean valid = true;
        if(this.fechaRetiro != null)
            if(!this.fechaRetiro.esFechaValida()){valid = false;}
            else{valid = !this.fechaRetiro.esFechaValida();}
        return valid;//COMPLETE
    }

    public boolean esEmpleadoValido() {
        if(this.fechaIngreso.esFechaValida() && this.fechaRetiro!= null){
            if(this.fechaRetiro.esFechaValida()) return true;
            return false;
        }
        
        if (this.fechaIngreso.esFechaValida()) return true;
        
        return false;
    }

    public boolean esEmpleadoLiquidable(Fecha fechaLiquidacion){
        int m_ingreso = this.fechaIngreso.getAnio() * 12 + this.fechaIngreso.getMes();
        int m_liquidacion = fechaLiquidacion.getAnio() * 12 + fechaLiquidacion.getMes();
        return this.esEmpleadoActivo() && this.esEmpleadoValido() && m_liquidacion - m_ingreso > 0;//COMPLETE
    }

    /**Metodo de acceso a la propiedad ingresos*/
    public double getIngresos(){
        return this.ingresos;
    }//end method getIngresos

    /**Metodo de modificación a la propiedad ingresos*/
    public void setIngresos(double ingresos){
        this.ingresos = ingresos;
    }//end method setIngresos

    /**Metodo de acceso a la propiedad descuentos*/
    public double getDescuentos(){
        return this.descuentos;
    }//end method getDescuentos

    /**Metodo de modificación a la propiedad descuentos*/
    public void setDescuentos(double descuentos){
        this.descuentos = descuentos;
    }//end method setDescuentos

    /**Metodo de acceso a la propiedad totalAPagar*/
    public double getTotalAPagar(){
        return this.totalAPagar;
    }//end method getTotalAPagar

    /**Metodo de modificación a la propiedad totalAPagar*/
    public void setTotalAPagar(double totalAPagar){
        this.totalAPagar = totalAPagar;
    }//end method setTotalAPagar

    /**Metodo de acceso a la propiedad fechaIngreso*/
    public Fecha getFechaIngreso(){
        return this.fechaIngreso;
    }//end method getFechaIngreso

    /**Metodo de modificación a la propiedad fechaIngreso*/
    public void setFechaIngreso(Fecha fechaIngreso){
        this.fechaIngreso = fechaIngreso;
    }//end method setFechaIngreso

    /**Metodo de acceso a la propiedad fechaRetiro*/
    public Fecha getFechaRetiro(){
        return this.fechaRetiro;
    }//end method getFechaRetiro

    /**Metodo de modificación a la propiedad fechaRetiro*/
    public void setFechaRetiro(Fecha fechaRetiro){
        this.fechaRetiro = fechaRetiro;
    }//end method setFechaRetiro

    public abstract double getProvisionRiesgos(Fecha fechaLiquidacion);
}