/*
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 * @author Colaborador: (Anthony Ardila 11519)
 */


public class Operario extends Empleado
{

    public Operario(){
    }

    public Operario(String cedula, String nombres, String apellidos, double salarioBase, int fechaIngreso, int fechaRetiro){
        //COMPLETE recuerde el super para herencia de constructores
        super(cedula, nombres, apellidos, salarioBase, fechaIngreso, fechaRetiro);
    }

    public double getProvisionRiesgos(Fecha fechaLiquidacion){
        this.liquidarEmpleado(fechaLiquidacion);
        return this.totalAPagar * 2.5 / 100;//COMPLETE
    }
}
